﻿using Steam.API.ViewModels;
using Steam.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Steam.API.Util
{
    public class Recommender
    {
        private SteamContext db = new SteamContext();

        public  IEnumerable<GameVM> GetRecommendedItems(int userId, float threshold = 0.5f, int max = -1, bool appendRandom = false)
        {

            var userIdealGame = GetIdealGame(userId);

            var gameItems = db.StoreItems.Select(x => new RecommenderItem()
            {
                Id = x.GameId,
                Price = x.Price,
                Rate = x.Game.Reviews.Average(r => r.Mark)
            }).ToList();

            var recommendedItems = new List<RecommenderItem>();


            if(!userIdealGame.IsEmpty)
            {
                foreach (var game in gameItems)
                {
                    if (GetSimilarities(userIdealGame, game) > threshold)
                    {
                        recommendedItems.Add(game);
                    }
                }
            
            }

            if (appendRandom)
            {
                recommendedItems.AddRange(gameItems.OrderBy(x => Guid.NewGuid()));
            }

            if (max != -1)
            {
                recommendedItems = recommendedItems.Take(max).ToList();
            }

            return RecommenderItemToGame(recommendedItems);
        }

        private IEnumerable<GameVM> RecommenderItemToGame(IEnumerable<RecommenderItem> recommended)
        {

            IEnumerable<int> gameIds = recommended.Select(x => x.Id).ToList();

            return db.Games.Where(x => gameIds.Contains(x.Id)).Select(g => new GameVM()
                                                                       {
                                                                           Id = g.Id,
                                                                           Name = g.Name,
                                                                           Esrb = g.Esrb.Name,
                                                                           AvgRate = g.Reviews.Average(r => r.Mark).ToString(),
                                                                           Genres = g.Genres.Select(h => h.Name).ToList(),
                                                                           Price = db.StoreItems.FirstOrDefault(s => s.GameId == g.Id).Price,
                                                                           OldPrice = db.StoreItems.FirstOrDefault(s => s.GameId == g.Id).OldPrice,
                                                                           IsOnSale = db.StoreItems.FirstOrDefault(s => s.GameId == g.Id).IsOnSale,
                                                                           Picture = g.Picture

                                                                       }).ToList();
        }


        private RecommenderItem GetIdealGame(int userId)
        {
            var avgLibraryRate = db.LibraryItems.Where(x => x.UserId == userId).Select(x => x.StoreItem.Game.Reviews.Average(r => r.Mark)).FirstOrDefault();
            var avgWishlistRate = db.WishItems.Where(x => x.UserId == userId).Select(x => x.Game.Reviews.Average(r => r.Mark)).FirstOrDefault();

            var avgRate = (avgLibraryRate + avgWishlistRate) / 2f;

            var avgLibraryPrice = db.LibraryItems.Where(x => x.UserId == userId).Select(x => x.StoreItem.Game.Reviews.Average(r => r.Mark)).FirstOrDefault();
            var avgWishlisPrice = db.WishItems.Where(x => x.UserId == userId).Select(x => x.Game.Reviews.Average(r => r.Mark)).FirstOrDefault();

            var avgPrice = (avgLibraryPrice / avgWishlisPrice) / 2f;

            return new RecommenderItem() { Id = -1, Rate = avgRate, Price = avgPrice };
        }

        private double GetSimilarities(RecommenderItem item1, RecommenderItem item2)
        {

            try
            {
                return item1 * item2 / (Math.Sqrt(item1 * item1) * Math.Sqrt(item2 * item2));

            }
            catch (Exception)
            {

                return 0;
            }

        }

        class RecommenderItem
        {
            public int Id;
            public double Rate;
            public double Price;

            public double [] ToArray()
            {
                return new double [] { Rate, Price };
            }

            public static double operator *(RecommenderItem r1, RecommenderItem r2)
            {
                return r1.Rate * r2.Rate + r1.Price * r2.Price;
            }

            public bool IsEmpty
            {
                get { return Rate == 0 || Price == 0; }
            }

        }

    }
}