﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Steam.API.Util
{
    public class Helper
    {
        public static int? GetUserIdFromHeader(HttpRequestMessage Request)
        {

            var headers = Request.Headers;

            if (headers.Contains("UserID"))
            {
                try
                {
                    return int.Parse(headers.GetValues("UserID").First());
                }
                catch (Exception)
                {

                    return null;
                }
            }
            return null;
        }
    }
}