﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Steam.API.Util
{
    public static class KeyGenerator
    {
        private static readonly Random Random = new Random();
        public static string GetKey()
        {
            string Key = string.Empty;

           
            for (int i = 0; i < 4; i++)
            {
                Key += Random.Next(1000, 9999);

                if (i != 3)
                    Key += "-";
            }

            return Key;
        }
    }
}