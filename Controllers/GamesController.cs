﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Steam.DAL;
using Steam.Domain;
using Steam.API.ViewModels;
using Steam.API.ViewModels.HCI;
using Steam.API.Util;

namespace Steam.API.Controllers
{
    public class GamesController : ApiController
    {
        private SteamContext db = new SteamContext();

        // GET: api/Games
        public IQueryable<Game> GetGames()
        {
            return db.Games;
        }
        // GET: api/Games/Store
        [HttpGet]
        public IEnumerable<GameVM> Store()
        {
            var StoreItems = db.StoreItems.Where(s => s.IsActive)
                              .Include(g => g.Game)
                              .Include(g => g.Game.Genres)
                              .Include(g => g.Game.Esrb)
                              .Include(g => g.Game.Reviews)
                              .Select(x => new GameVM()
                              {
                                  Id = x.Id,
                                  Name = x.Game.Name,
                                  Esrb = x.Game.Esrb.Name,
                                  AvgRate = x.Game.Reviews.Average(r => r.Mark).ToString(),
                                  Genres = x.Game.Genres.Select(g => g.Name).ToList(),
                                  Price = x.Price,
                                  OldPrice = x.OldPrice,
                                  IsOnSale = x.IsOnSale,
                                  Picture = x.Game.Picture
                              }).ToList();

            

            return StoreItems;
        }
        // GET: api/Games/Wishlist
        [HttpGet]
        public IEnumerable<GameVM> Wishlist(int id)
        {
            var WishlistItems = db.WishItems
                                  .Where(g=> g.UserId == id)
                                  .Include(g => g.Game)
                                  .Include(g => g.Game.Genres)
                                  .Include(g => g.Game.Esrb)
                                  .Include(g => g.Game.Reviews)
                                  .Select(x => new GameVM()
                                  {

                                      Id = x.Id,
                                      Name = x.Game.Name,
                                      Esrb = x.Game.Esrb.Name,
                                      AvgRate = x.Game.Reviews.Average(r => r.Mark).ToString(),
                                      Genres = x.Game.Genres.Select(g => g.Name).ToList(),
                                      Price = db.StoreItems.FirstOrDefault( s => s.GameId == id).Price,
                                      OldPrice = db.StoreItems.FirstOrDefault(s => s.GameId == id).OldPrice,
                                      IsOnSale = db.StoreItems.FirstOrDefault(s => s.GameId == id).IsOnSale,
                                      Picture = x.Game.Picture


                                  }).ToList();

            return WishlistItems;
        }
        // GET: api/Games/Library
        [HttpGet]
        public IEnumerable<GameVM> Library(int id = 1)
        {
           

            var StoreItems = db.LibraryItems.Where( l => l.Id == id)

                                            .Select(x => new GameVM()
                                            {
                                                Id = x.Id,
                                                Name = x.StoreItem.Game.Name,
                                                Esrb = x.StoreItem.Game.Esrb.Name,
                                                AvgRate = x.StoreItem.Game.Reviews.Average(r => r.Mark).ToString(),
                                                Genres = x.StoreItem.Game.Genres.Select(g => g.Name).ToList(),
                                                Price = x.StoreItem.Price,
                                                OldPrice = x.StoreItem.OldPrice,
                                                IsOnSale = x.StoreItem.IsOnSale,
                                                Picture = x.StoreItem.Game.Picture

                                            }).ToList();

            return StoreItems;
        }

        // GET: api/Games/Recommended
        [HttpGet]
        public IEnumerable<GameVM> Recommended(int id = 1)
        {
            return new Recommender().GetRecommendedItems(id, threshold: 0.2f, max: 10, appendRandom: true);
        }








        //FOR USERS
        // GET: api/Games/5
        [ResponseType(typeof(Game))]
        public IHttpActionResult GetGameForUser(int id)
        {


            var GameRaw = db.StoreItems.Where(x => x.GameId == id)
                                                 .Select(x => new 
                                                 {
                                                     GameId = x.Game.Id,
                                                     Name = x.Game.Name,
                                                     Description = x.Game.Description,
                                                     WebsiteLink = x.Game.WebsiteLink,
                                                     Picture = x.Game.Picture,
                                                     
                                                     IsOnSale = x.IsOnSale,
                                                     Price = x.Price,
                                                     OldPrice = x.OldPrice,
                                                     AddedDate = x.AddedDate,

                                                     Comments = x.Game.Comments.Select(c => new CommentVM() 
                                                       {
                                                            Id = c.Id,
                                                            Content = c.Content,
                                                            DateTime = c.DateTime,
                                                            User = c.User.FirstName + " " + c.User.LastName
                                                       }).ToList(),



                                                     Esrb = x.Game.Esrb.Name,
                                                     AuthorName = x.Game.Autor.Name,
                                                     Genres  = x.Game.Genres,

                                                     AvgRate = x.Game.Reviews.Average(  r => r.Mark),
                                                     IsActive = x.IsActive

                                                 }).FirstOrDefault();


            var Game = new GameDetailsAllVM();

            Game.GameId = GameRaw.GameId;
            Game.Name = GameRaw.Name;
            Game.Description = GameRaw.Description;
            Game.AutorName = GameRaw.AuthorName;
            Game.AddedDate = GameRaw.AddedDate;
            Game.AvgRate = (float) GameRaw.AvgRate;
            Game.WebsiteLink = GameRaw.WebsiteLink;
            Game.Esrb = GameRaw.Esrb;
            Game.IsActive = GameRaw.IsActive;
            Game.Comments = GameRaw.Comments;

            Game.Price = GameRaw.Price;
            Game.OldPrice = GameRaw.OldPrice;
            Game.IsOnSale = GameRaw.IsOnSale;
            Game.Genres = GameRaw.Genres.Select(x => x.Name).ToList();
            //Game.Comments = GameRaw.Comments.Select(x => new CommentVM() 
            //                                       {
            //                                            Id = x.Id,
            //                                            Content = x.Content,
            //                                            DateTime = x.DateTime,
            //                                            User = x.User.FirstName + " " + x.User.LastName
            //                                       }).ToList();
            

            var keyCode = db.LibraryItems.Where(x => x.UserId == 1 && x.StoreItem.GameId == Game.GameId)
                                            .Select(x => x.KeyCode ).FirstOrDefault();

            if (keyCode != "")
            {
                Game.IsPurchased = true;
                Game.KeyCode = keyCode;
            }
            Game.IsOnWishlist = db.WishItems.FirstOrDefault(x => x.GameId == id && x.UserId == 1) == null ? false : true;

            /*
            Game.Genres = string.Join(", ", Game.GenresList);
            Game.IsPurchased =  db.LibraryItems.Where( x => x.UserId == 1)
                                               .Include( x => x.StoreItem)
                                               .FirstOrDefault(x => x.StoreItem.GameId == id) == null ? false : true;
            */
            return Ok(Game);
        }

        [ResponseType(typeof(GameDetailsAllVM))]
        public IHttpActionResult GetGame(int id)
        {


            var userId = 1;

            var GameRaw = db.StoreItems.Where(x => x.GameId == id)
                                                 .Select(x => new
                                                 {
                                                     GameId = x.Game.Id,
                                                     Name = x.Game.Name,
                                                     Description = x.Game.Description,
                                                     WebsiteLink = x.Game.WebsiteLink,
                                                     Picture = x.Game.Picture,

                                                     IsOnSale = x.IsOnSale,
                                                     Price = x.Price,
                                                     OldPrice = x.OldPrice,
                                                     AddedDate = x.AddedDate,

                                                     Comments = x.Game.Comments.Select(c => new CommentVM()
                                                     {
                                                         Id = c.Id,
                                                         Content = c.Content,
                                                         DateTime = c.DateTime,
                                                         User = c.User.FirstName + " " + c.User.LastName
                                                     }).ToList(),
                                                     Esrb = x.Game.Esrb.Name,
                                                     AuthorName = x.Game.Autor.Name,
                                                     Genres = x.Game.Genres,

                                                     AvgRate = x.Game.Reviews.Average(r => r.Mark),
                                                     IsActive = x.IsActive,

                                                     //User Specific
                                                     WishlistItem = db.WishItems.FirstOrDefault( w => w.GameId == x.GameId &&  w.UserId == userId),
                                                     LibraryItem = db.LibraryItems.FirstOrDefault( l => l.StoreItem.GameId == x.GameId && l.UserId == userId)

                                                 }).FirstOrDefault();


            var Game = new GameDetailsAllVM();

            Game.GameId = GameRaw.GameId;
            Game.Name = GameRaw.Name;
            Game.Description = GameRaw.Description;
            Game.AutorName = GameRaw.AuthorName;
            Game.AddedDate = GameRaw.AddedDate;
            Game.AvgRate = (float)GameRaw.AvgRate;
            Game.WebsiteLink = GameRaw.WebsiteLink;
            Game.Esrb = GameRaw.Esrb;
            Game.IsActive = GameRaw.IsActive;
            Game.Comments = GameRaw.Comments;

            Game.Price = GameRaw.Price;
            Game.OldPrice = GameRaw.OldPrice;
            Game.IsOnSale = GameRaw.IsOnSale;
            Game.Genres = GameRaw.Genres.Select(x => x.Name).ToList();

            Game.IsOnWishlist = GameRaw.WishlistItem != null ? true : false;
            Game.IsPurchased = GameRaw.LibraryItem != null ? true : false;

            return Ok(Game);
        }


        [HttpPost]
        [ResponseType(typeof(void))]
        public IHttpActionResult PostGameChanges(GameDetailsAllVM game)
        {
            /*
            StoreItem storeItem = db.StoreItems.Where(x => x.GameId == game.GameId).FirstOrDefault();
            
            storeItem.IsOnSale = game.IsOnSale;
            storeItem.IsActive = game.IsActive;

            storeItem.Price = game.Price;
            storeItem.OldPrice = game.OldPrice;

            Game gameItem = db.Games.Where(x => x.Id == game.GameId).FirstOrDefault();

            gameItem.Name = game.Name;
            gameItem.Description = game.Description;
            gameItem.Autor = db.Authors.Where(x => x.Id == game.AuthorId).FirstOrDefault();
            gameItem.Esrb = db.ESRBs.Where(x => x.Name == game.Esrb).FirstOrDefault();
            gameItem.Genres = db.Genres.Where(x => game.Genres.Contains(x.Name)).ToList();
            gameItem.Picture = game.Picture;


            db.Entry(gameItem).State = EntityState.Modified;
            db.Entry(storeItem).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {

            }
            */



            db.Games.Where(x => x.Id == 1).FirstOrDefault().Picture = game.Picture;
            db.SaveChanges();


            return StatusCode(HttpStatusCode.Accepted);
        }

        // PUT: api/Games/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutGame(int id, Game game)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != game.Id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(game).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!GameExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        // POST: api/Games
        //[ResponseType(typeof(Game))]
        //public IHttpActionResult PostGame(Game game)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Games.Add(game);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = game.Id }, game);
        //}


        [HttpPost]
        public bool Buy(BuyHCI buyItem)
        {



            try
            {
                var libraryItem = new LibraryItem();
                
                libraryItem.KeyCode = Util.KeyGenerator.GetKey();
                libraryItem.PurchaseDate = DateTime.Now;
                libraryItem.UserId = buyItem.UserId;
                libraryItem.StoreItemId = db.StoreItems.First(x => x.GameId == buyItem.GameId).Id;

                db.LibraryItems.Add(libraryItem);
                db.SaveChanges();
            }
            catch (Exception)
            {

                return false;
            }

            return true;

        }
        [HttpPost]
        public bool AddToWishlist(WishlistHCI wishlistItem)
        {
            try
            {
                var item = new WishItem();

                item.AddedDate = DateTime.Now;
                item.UserId = wishlistItem.UserId;
                item.GameId = wishlistItem.GameId;
            
                db.WishItems.Add(item);
                db.SaveChanges();
            }
            catch (Exception)
            {

                return false;
            }

            return true;
        }


        [HttpPost]
        public bool RemoveFromWishlist(WishlistHCI wishlistItem)
        {
            try
            {
                var item = db.WishItems.First(x => x.GameId == wishlistItem.GameId && x.UserId == wishlistItem.UserId);

                db.WishItems.Remove(item);
                db.SaveChanges();
            }
            catch (Exception)
            {

                return false;
            }

            return true;
        }




        // DELETE: api/Games/5
        [ResponseType(typeof(Game))]
        public IHttpActionResult DeleteGame(int id)
        {
            Game game = db.Games.Find(id);
            if (game == null)
            {
                return NotFound();
            }

            db.Games.Remove(game);
            db.SaveChanges();

            return Ok(game);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GameExists(int id)
        {
            return db.Games.Count(e => e.Id == id) > 0;
        }
    }
}