﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Steam.DAL;
using Steam.Domain;
using Steam.API.ViewModels;

namespace Steam.API.Controllers
{
    public class UsersController : ApiController
    {
        private SteamContext db = new SteamContext();


        public IHttpActionResult CreateNew(User user)
        {

            db.Users.Add(user);
            db.SaveChanges();

            return Ok();
        
        }
        [HttpPost]
        public UserHCI CheckEmailAndPassword(UserHCI model)
        {
            return new UserHCI() { Id = 1, Email = "hamza@fourgamestudio.com", FirstName = "Anaid", LastName = "Fazlic" };

        }


        [HttpGet]
        [ResponseType(typeof(User))]
        public IHttpActionResult Check(string id)
        {

            var user = db.Users.Where(x => x.Email == id).SingleOrDefault();

            return Ok(user);
        }

        // GET: api/Users
        public IQueryable<Steam.ViewModels.UserDetails> GetUsers()
        {
            return db.Users.Select(x => new Steam.ViewModels.UserDetails() 
            { 
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                NumberOfGamesOnWishlist =  1,//db.WishItems.Where(w => w.)
                NumberOfPurchasedGames = 2,
                IsActive = x.IsActive
            });
        }

        [ResponseType(typeof(IQueryable<Steam.ViewModels.UserDetails>))]
        [HttpGet]
        public IHttpActionResult Search(string id)
        {
            var users = db.Users.Where( u => u.FirstName.ToLower().Contains(id.ToLower()) || u.LastName.ToLower().Contains(id.ToLower()))
                                .Select(x => new Steam.ViewModels.UserDetails()
                                {
                                    Id = x.Id,
                                    FirstName = x.FirstName,
                                    LastName = x.LastName,
                                    NumberOfGamesOnWishlist = 1,//db.WishItems.Where(w => w.)
                                    NumberOfPurchasedGames = 2,
                                    IsActive = true
                                });

            return Ok(users);
        }




        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult GetUser(int id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.Id)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users
        [ResponseType(typeof(User))]
        public IHttpActionResult PostUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Users.Add(user);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = user.Id }, user);
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(int id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.Users.Count(e => e.Id == id) > 0;
        }
    }
}