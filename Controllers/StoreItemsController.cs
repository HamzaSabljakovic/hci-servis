﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Steam.DAL;
using Steam.Domain;
using Steam.API.ViewModels;

namespace Steam.API.Controllers
{
    public class StoreItemsController : ApiController
    {
        private SteamContext db = new SteamContext();

        // GET: api/StoreItems

        public IEnumerable<GameDetailsVM> GetStoreItems()
        {
            return db.StoreItems.Select(x => new GameDetailsVM() 
            {
                GameId = x.GameId,
                Name = x.Game.Name,
                Esrb =  x.Game.Esrb.Name,
                IsOnSale = x.IsOnSale,
                OldPrice = x.OldPrice,
                Price = x.Price,
                Author = x.Game.Autor.Name
                
            }).ToList();
        }

        public IEnumerable<LibraryItem> GetMostPopularStoreItems()
        {
            return null;




            /*
            var mostPopular = db.LibraryItems.GroupBy(x => x.StoreItemId)
                            .OrderByDescending(x => x.Count())
                            .Select(x => x.);
             */
            /*
             var mostFollowedQuestions = db.LibraryItems
                                           .GroupBy(q => q.StoreItemId)
                                           .OrderByDescending(gp => gp.Count())
                                           .Take(10)
                                           .Select(g => g).ToList();


            return mostFollowedQuestions;
             * 
             */

            //return null;

            //db.LibraryItems.Join( )


            /*

            //Right join
            var query = from storeItems in db.StoreItems
                        join popularItem in mostPopular on storeItems.Id equals popularItem.StoreItemId into mostPopularStoreItems
                        from mostPopularSI in mostPopularStoreItems.DefaultIfEmpty()
                        select new GameDetailsVM()
                        {
                            GameId = mostPopularSI.StoreItem.Id,
                            Author = mostPopularSI.StoreItem.Game.Autor.Name,
                            Esrb = mostPopularSI.StoreItem.Game.Esrb.Name,
                            IsOnSale = mostPopularSI.StoreItem.IsOnSale,
                            Name = mostPopularSI.StoreItem.Game.Name,
                            OldPrice = mostPopularSI.StoreItem.OldPrice,
                            Price = mostPopularSI.StoreItem.Price
                        };


            return query;
             * 
             * */




            //return db.StoreItems..Select(x => new GameDetailsVM()
            //{
            //    GameId = x.GameId,
            //    Name = x.Game.Name,
            //    Esrb = x.Game.Esrb.Name,
            //    IsOnSale = x.IsOnSale,
            //    OldPrice = x.OldPrice,
            //    Price = x.Price,
            //    Author = x.Game.Autor.Name

            //}).ToList();
        }



        // PUT: api/StoreItems/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutStoreItem(int id, StoreItem storeItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != storeItem.Id)
            {
                return BadRequest();
            }

            db.Entry(storeItem).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StoreItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        [ResponseType(typeof(IQueryable<GameDetailsVM>))]
        [HttpGet]
        public IHttpActionResult Search(string id)
        {
            var storeItems = db.StoreItems.Where(x => x.Game.Name.ToUpper().
                Contains(id.ToUpper()) || x.Game.Autor.Name.ToUpper()
                .Contains(id.ToUpper())).Select(x => new GameDetailsVM() 
            {
                GameId = x.GameId,
                Name = x.Game.Name,
                Esrb =  x.Game.Esrb.Name,
                IsOnSale = x.IsOnSale,
                OldPrice = x.OldPrice,
                Price = x.Price,
                Author = x.Game.Autor.Name
                
            }).ToList();

           return Ok(storeItems);
        }

        [ResponseType(typeof(IQueryable<GameDetailsVM>))]
        [HttpGet]
        public IHttpActionResult MostPopular()
        {

           var rawData = db.StoreItems.Select(x => new 
                {
                    GameId = x.GameId,
                    Name = x.Game.Name,
                    Esrb = x.Game.Esrb.Name,
                    IsOnSale = x.IsOnSale,
                    OldPrice = x.OldPrice,
                    Price = x.Price,
                    Author = x.Game.Autor.Name,
                    Popularity = db.LibraryItems.Count( l=> l.StoreItem.GameId == x.GameId)

                }).ToList();


           rawData = rawData.OrderByDescending(h => h.Popularity).ToList();

           var storeItems = rawData.Select(x => new GameDetailsVM()
               {
                   GameId = x.GameId,
                   Name = x.Name,
                   Esrb =  x.Esrb,
                   IsOnSale = x.IsOnSale,
                   OldPrice = x.OldPrice,
                   Price = x.Price,
                   Author = x.Author

               }).ToList();

            return Ok(storeItems);
        }
        


        // POST: api/StoreItems
        [ResponseType(typeof(StoreItem))]
        public IHttpActionResult PostStoreItem(StoreItem storeItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            storeItem.Game = db.Games.Where(x =>x.Id == storeItem.GameId).FirstOrDefault();

            db.StoreItems.Add(storeItem);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = storeItem.Id }, storeItem);
        }

        // DELETE: api/StoreItems/5
        [ResponseType(typeof(StoreItem))]
        public IHttpActionResult DeleteStoreItem(int id)
        {
            StoreItem storeItem = db.StoreItems.Find(id);
            if (storeItem == null)
            {
                return NotFound();
            }

            db.StoreItems.Remove(storeItem);
            db.SaveChanges();

            return Ok(storeItem);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StoreItemExists(int id)
        {
            return db.StoreItems.Count(e => e.Id == id) > 0;
        }
    }
}