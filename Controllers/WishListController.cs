﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Steam.DAL;
using Steam.Domain;
using Steam.API.ViewModels;
using Steam.API.Util;

namespace Steam.API.Controllers
{
    public class WishListController : ApiController
    {
        private SteamContext db = new SteamContext();

        // GET: api/WishList
        public List<GameVM> GetWishItems()
        {


            //var wishitem = new WishItem() { AddedDate = DateTime.Now, UserId = 1, GameId = 1 };

            //db.WishItems.Add(wishitem);
            //db.SaveChanges();


            /*
            int ? id = Helper.GetUserIdFromHeader(Request);

            if (id != null)
            {
                var gameItems = db.LibraryItems.Where( l => l.UserId == id)
                                                .Select(x => new GameVM()
                                                {
                                                    Id = x.StoreItem.GameId,
                                                    Name = x.StoreItem.Game.Name,
                                                    Esrb = x.StoreItem.Game.Esrb.Name,
                                                    AvgRate = x.StoreItem.Game.Reviews.Average(r => r.Mark).ToString(),
                                                    Genres = x.StoreItem.Game.Genres.Select(g => g.Name).ToList(),

                                                }).ToList();
                return gameItems;
            }

            return null;
             * 
             * */

            var gameItems = db.WishItems.Where(u => u.UserId == 1 ).Select(x => new GameVM()
                                               {
                                                     Id = x.GameId,
                                                     Name = x.Game.Name,
                                                     AvgRate = db.Reviews.Where(g => g.GameId == x.GameId ).Average( a => a.Mark).ToString(),
                                                     Esrb = db.ESRBs.Where( g => g.Id == x.Game.EsrbId).FirstOrDefault().Name,
                                                     IsOnSale = db.StoreItems.Where( s => s.GameId == x.GameId).FirstOrDefault().IsOnSale,
                                                     OldPrice = db.StoreItems.Where(s => s.GameId == x.GameId).FirstOrDefault().OldPrice,
                                                     Price = db.StoreItems.Where(s => s.GameId == x.GameId).FirstOrDefault().Price
                                               }).ToList();

            return gameItems;
        }

        // PUT: api/WishList/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutWishItem(int id, WishItem wishItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != wishItem.Id)
            {
                return BadRequest();
            }

            db.Entry(wishItem).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WishItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/WishList
        [ResponseType(typeof(WishItem))]
        public IHttpActionResult PostWishItem(WishItem wishItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.WishItems.Add(wishItem);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = wishItem.Id }, wishItem);
        }

        // DELETE: api/WishList/5
        [ResponseType(typeof(WishItem))]
        public IHttpActionResult DeleteWishItem(int id)
        {
            WishItem wishItem = db.WishItems.Find(id);
            if (wishItem == null)
            {
                return NotFound();
            }

            db.WishItems.Remove(wishItem);
            db.SaveChanges();

            return Ok(wishItem);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool WishItemExists(int id)
        {
            return db.WishItems.Count(e => e.Id == id) > 0;
        }
    }
}