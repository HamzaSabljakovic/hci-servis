﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Steam.API.ViewModels;
using Steam.DAL;
using Steam.API.Util;

namespace Steam.API.Controllers
{
    public class StatisticsController : ApiController
    {
        private SteamContext db = new SteamContext();

        // GET: api/Statistics/1
        [HttpGet]
        [ResponseType(typeof(StatisticsVM))]
        public IHttpActionResult GetStatisticsVM(int id = 1)
        {

                var Statistics = db.LibraryItems.Where(x => x.UserId == id)
                                                .Select(s => new StatisticsVM()
                                                {
                                                    CommentsCount = s.StoreItem.Game.Comments.Count(),
                                                    ReviewsCount = s.StoreItem.Game.Reviews.Count(),
                                                    AvgRate = s.StoreItem.Game.Reviews.Average(a => a.Mark),
                                                    MoneySpent = db.LibraryItems.Sum(l => l.StoreItem.Price),//Provjeriti treba li uslov 
                                                    PurchasedGamesCount = db.LibraryItems.Where(l => l.UserId == id).Count(),
                                                    WishListItemsCount = db.WishItems.Count(),//Uslov kad se uradi update baze

                                                    Name = s.User.FirstName + s.User.LastName,
                                                    Email = s.User.Email

                                                }).FirstOrDefault();

                return Ok(Statistics);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}