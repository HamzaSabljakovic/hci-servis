﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Steam.DAL;
using Steam.Domain;
using Steam.API.ViewModels;

namespace Steam.API.Controllers
{
    public class AuthorsController : ApiController
    {
        private SteamContext db = new SteamContext();

        public IEnumerable<AuthorVM> GetAuthors()
        { 
            var Authors = new List<AuthorVM>();

            Authors = db.Authors.Select(x => new AuthorVM()
            {
                Id =  x.Id,
                Name = x.Name,
                Picture = x.Picture,
                Description =  x.Description,
                WebsiteLink = x.WebsiteLink,
                NumberOfPublishedGames = x.Games.Count()

            }).ToList();

            return Authors;
        }

        // GET: api/Authors/5
        
        [ResponseType(typeof(AuthorDetailsVM))]
        [HttpGet]
        public IHttpActionResult GetAuthor(int id)
        {
            var Author = db.Authors.Where(a => a.Id == id)
                                   .Select(x => new AuthorDetailsVM()
                                   {
                                       Author = new AuthorVM()
                                       {
                                           Id = x.Id,
                                           Name = x.Name,
                                           Description = x.Description,
                                           WebsiteLink = x.WebsiteLink,
                                           Picture = x.Picture

                                       }
                                   }).FirstOrDefault();

            if (Author == null)
            {
                return NotFound();
            }
        
            Author.Games = db.Games.Where(g => g.AuthorId == id)
                                        .Select(x => new GameVM()
                                        {
                                            Id = x.Id,
                                            Name = x.Name,
                                            Esrb = x.Esrb.Name,
                                            AvgRate = x.Reviews.Average(r => r.Mark).ToString(),
                                            Genres = x.Genres.Select(g => g.Name).ToList(),


                                        }).ToList();
            return Ok(Author);
        }

        [ResponseType(typeof(IQueryable<string>))]
        [HttpGet]
        public IHttpActionResult GetAuthorsName()
        {
            var Authors = db.Authors.Select( x => x.Name);

            return Ok(Authors);
        }


        [ResponseType(typeof(IQueryable< AuthorVM>))]
        [HttpGet]
        public IHttpActionResult Search(string id)
        {
            var Author = db.Authors.Where(a => a.Name.ToUpper().Contains(id.ToUpper()));
                                  
                
                
                /*.Select(x => new AuthorDetailsVM()
                                   {
                                       Author = new AuthorVM()
                                       {
                                           Id = x.Id,
                                           Name = x.Name,
                                           Description = x.Description,
                                           WebsiteLink = x.WebsiteLink,
                                           Picture = x.Picture

                                       }
                                   });*/

            if (Author == null)
            {
                return NotFound();
            }
            /*
            Author.Games = db.Games.Where(g => g.AuthorId == id)
                                        .Select(x => new GameVM()
                                        {
                                            Id = x.Id,
                                            Name = x.Name,
                                            Esrb = x.Esrb.Name,
                                            AvgRate = x.Reviews.Average(r => r.Mark).ToString(),
                                            Genres = x.Genres.Select(g => g.Name).ToList(),


                                        }).ToList();
             * 
             * */
            return Ok(Author);
        }

        [ResponseType(typeof(IQueryable<AuthorVM>))]
        [HttpGet]
        public IHttpActionResult MostPopular()
        {



            var rawData = db.Authors.Select(x => new
            {
                Author = x,
                Popularity = db.LibraryItems.Count(l => l.StoreItem.Game.AuthorId == x.Id)
            }).ToList();


            rawData = rawData.OrderByDescending(h => h.Popularity).ToList();

            var authorItems = rawData.Select(x => new AuthorVM()
            {
                Id = x.Author.Id,
                Description = x.Author.Description,
                Name = x.Author.Name,
                Picture = x.Author.Picture,
                WebsiteLink = x.Author.WebsiteLink

            }).ToList();

            return Ok(authorItems);

        }



        // PUT: api/Authors/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAuthor(int id, Author author)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != author.Id)
            {
                return BadRequest();
            }

            db.Entry(author).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AuthorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Authors
        [ResponseType(typeof(Author))]
        public IHttpActionResult PostAuthor(Author author)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Authors.Add(author);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = author.Id }, author);
        }

        // DELETE: api/Authors/5
        [ResponseType(typeof(Author))]
        public IHttpActionResult DeleteAuthor(int id)
        {
            Author author = db.Authors.Find(id);
            if (author == null)
            {
                return NotFound();
            }

            db.Authors.Remove(author);
            db.SaveChanges();

            return Ok(author);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AuthorExists(int id)
        {
            return db.Authors.Count(e => e.Id == id) > 0;
        }
    }
}