﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Steam.API.ViewModels
{
	public class GameDetailsVM
	{
        public int GameId { get; set; }
        public string Name { get; set; }
        public string Esrb { get; set; }
        public double Price { get; set; }
        public double OldPrice { get; set; }
        public bool IsOnSale { get; set; }
        public string Author { get; set; }
	}
}