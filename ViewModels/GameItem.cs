﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Steam.API.ViewModels
{
    public class GameItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Esrb { get; set; }
        public int AvgRate { get; set; }

    }
}