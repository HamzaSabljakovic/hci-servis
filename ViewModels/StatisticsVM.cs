﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Steam.API.ViewModels
{
    public class StatisticsVM
    {
        public int PurchasedGamesCount { get; set; }
        public int WishListItemsCount { get; set; }

        public int ReviewsCount { get; set; }
        public double AvgRate { get; set; }
        public int CommentsCount { get; set; }

        public DateTime JoinDate { get; set; }
        public double MoneySpent { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }

    }
}