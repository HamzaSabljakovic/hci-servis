﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Steam.API.ViewModels.HCI
{
    public class WishlistHCI
    {
        public int GameId { get; set; }
        public int UserId { get; set; }
    }
}