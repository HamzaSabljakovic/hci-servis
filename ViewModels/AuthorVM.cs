﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Steam.API.ViewModels
{
    public class AuthorVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string WebsiteLink { get; set; }
        public int NumberOfPublishedGames { get; set; }
        public byte[] Picture { get; set; }
    }
}