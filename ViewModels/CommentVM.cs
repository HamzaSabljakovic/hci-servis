﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Steam.API.ViewModels
{
    public class CommentVM
    {
        public int Id { get; set; }
        public string User { get; set; }
        public string Content { get; set; }
        public DateTime DateTime { get; set; }
    }
}