﻿using Steam.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Steam.API.ViewModels
{
    public class GameDetailsAllVM
    {
        public float Price { get; set; }
        public bool IsOnSale { get; set; }
        public float OldPrice { get; set; }
        public DateTime AddedDate { get; set; }
        public bool IsActive { get; set; }

        public int GameId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string WebsiteLink { get; set; }

        public int AuthorId { get; set; }
        public string  AutorName { get; set; }

        public string Esrb { get; set; }
        public byte[] Picture { get; set; }

        public virtual ICollection<CommentVM> Comments { get; set; }
        public List<string>  Genres { get; set; }
        public float AvgRate { get; set; }

        public bool IsPurchased { get; set; }
        public string KeyCode { get; set; }

        public bool IsOnWishlist { get; set; }
    }
}