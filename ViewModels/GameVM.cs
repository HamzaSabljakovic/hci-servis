﻿using Steam.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Steam.API.ViewModels
{
    public class GameVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Esrb { get; set; }
        public string AvgRate { get; set; }
        //public IEnumerable<Genre> Genres { get; set; }
        public IEnumerable<string> Genres { get; set; }
        public double Price { get; set; }
        public double OldPrice { get; set; }
        public bool IsOnSale { get; set; }
        public byte[] Picture { get; set; }
        

    }
}