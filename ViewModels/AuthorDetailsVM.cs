﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Steam.API.ViewModels
{
    public class AuthorDetailsVM
    {
        public AuthorVM Author { get; set; }
        public IEnumerable<GameVM> Games { get; set; }
    }
}